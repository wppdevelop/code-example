<?php

namespace App\Tests\unit\Service;

use App\Service\FormatterService;
use App\Util\HtmlFormatter\HtmlDecoratorInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormatterServiceTest extends Unit
{
    /** @var MockObject */
    private $parameterBag;

    protected function _before()
    {
        $this->parameterBag = $this->getMockBuilder(ParameterBagInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function _after()
    {
    }

    public function testGetGroup()
    {
        $this->parameterBag->expects($this->any())
            ->method('get')
            ->willReturn(
                $this->getModule('Symfony')->_getContainer()->get('service_container')->getParameter('html_formatter')
            );

        $formatterService = new FormatterService($this->parameterBag);
        $this->assertInstanceOf(HtmlDecoratorInterface::class, $formatterService->getGroup('default'));
    }

    public function testGetGroupNotFound()
    {
        $this->parameterBag->expects($this->any())
            ->method('get')
            ->willReturn([
                'groups' => [
                    'default' => []
                ]
            ]);
        $this->expectException(RuntimeException::class);
        $formatterService = new FormatterService($this->parameterBag);
        $formatterService->getGroup('fake');
    }

    public function testGetGroupEmptyFormttersInConfig()
    {
        $this->parameterBag
            ->expects($this->once())
            ->method('get')
            ->willReturn([
                'groups' => [
                    'fake' => []
                ]
            ]);
        $formatterService = new FormatterService($this->parameterBag);
        $formatterService->getGroup('fake');
    }
}
