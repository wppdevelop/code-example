<?php

namespace App\Tests\unit\MessageHandler\Docs;

use App\Codeception\Unit\UnitTrait;
use App\Entity\Docs\DocItem;
use App\Message\Docs\ItemFormatter;
use App\MessageHandler\Docs\ItemFormatterHandler;
use App\Repository\Docs\DocItemRepository;
use App\Service\FormatterService;
use App\Util\DOM\FactoryWrapper;
use Codeception\Test\Unit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ItemFormatterHandlerTest extends Unit
{
    use UnitTrait;

    public function testInvoke()
    {
        $expected = <<<HTML

<h2 class="section-title-2">h2</h2>
<table class="table table-striped boxed"></table>
HTML;

        $actual = <<<HTML
<h1>h1</h1>
<h2>h2</h2>
<table></table>
HTML;

        $docItem = (new DocItem())
            ->setBodyHtml($actual);

        $docItemMock = $this->getMockBuilder(DocItemRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $docItemMock
            ->expects($this->once())
            ->method('findOneByDocLanguageVersionItemSlug')
            ->willReturn($docItem);

        $em = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parameterBag = $this->getMockBuilder(ParameterBagInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parameterBag->expects($this->any())
            ->method('get')
            ->willReturn(
                $this->getModule('Symfony')->_getContainer()->get('service_container')->getParameter('html_formatter')
            );

        $formatterServise = new FormatterService($parameterBag);
        $itemFormatterHandler = new ItemFormatterHandler($docItemMock, $em, $formatterServise, new FactoryWrapper());
        $itemFormatterHandler(new ItemFormatter('symfony', 'en', '4.3', 'test'));

        $this->assertSame($expected, $docItem->getBodyHtml());
    }
}
