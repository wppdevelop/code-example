<?php

namespace App\Tests\unit\Util\HtmlFormatter;

use App\Util\HtmlFormatter\HeadingAddCssClassesFormatter;
use App\Util\HtmlFormatter\Html;
use Codeception\Test\Unit;

class HeadingAddCssClassesFormatterTest extends AbstractFormatter
{
    protected function _before(): void
    {
        $this->formatter = new HeadingAddCssClassesFormatter(new Html());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function htmlDataProvider(): array
    {
        return [
            ['<h2 class="section-title-2">h2</h2>', '<h2>h2</h2>'],
            ['<div><h3 class="section-title-2">h2</h3></div>', '<div><h3 class="test-class">h2</h3></div>']
        ];
    }
}
