<?php

namespace App\Tests\unit\Util\HtmlFormatter;

use App\Util\HtmlFormatter\Html;
use App\Util\HtmlFormatter\TableAddCssClassesFormatter;

class TableAddCssClassesFormatterTest extends AbstractFormatter
{
    protected function _before(): void
    {
        $this->formatter = new TableAddCssClassesFormatter(new Html());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function htmlDataProvider(): array
    {
        return [
            ['<table class="table table-striped boxed"></table>', '<table></table>'],
            ['<div><table class="table table-striped boxed"></table></div>', '<div><table class="test-class"></table></div>']
        ];
    }
}
