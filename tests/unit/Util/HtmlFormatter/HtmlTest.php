<?php

namespace App\Tests\unit\Util\HtmlFormatter;

use App\Util\HtmlFormatter\Html;

class HtmlTest extends AbstractFormatter
{
    protected function _before(): void
    {
        $this->formatter = new Html();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function htmlDataProvider(): array
    {
        return [
            ['<p>test</p>', '<p>test</p>']
        ];
    }
}
