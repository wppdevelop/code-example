<?php

namespace App\Tests\unit\Util\HtmlFormatter;

use App\Util\HtmlFormatter\H1RemoveFormatter;
use App\Util\HtmlFormatter\Html;

class H1RemoveFormatterTest extends AbstractFormatter
{
    protected function _before(): void
    {
        $this->formatter = new H1RemoveFormatter(new Html());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function htmlDataProvider(): array
    {
        return [
            ['', '<h1></h1>'],
            ['<div></div>', '<div><h1 class="test-class"></h1></div>']
        ];
    }
}
