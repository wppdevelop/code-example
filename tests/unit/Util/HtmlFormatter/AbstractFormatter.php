<?php

namespace App\Tests\unit\Util\HtmlFormatter;

use App\Util\DOM\FactoryWrapper;
use App\Util\HtmlFormatter\HtmlDecoratorInterface;
use Codeception\Test\Unit;

abstract class AbstractFormatter extends Unit
{
    /**
     * @var HtmlDecoratorInterface
     */
    protected $formatter;

    public function testContainsInstance(): void
    {
        $this->assertContainsOnlyInstancesOf(HtmlDecoratorInterface::class, [$this->formatter]);
    }

    abstract public function htmlDataProvider(): array;

    /**
     * @param string $expected
     * @param string $actual
     * @dataProvider htmlDataProvider
     * @throws \Exception
     */
    public function testFormatting(string $expected, string $actual): void
    {
        $dom = (new FactoryWrapper())->build('crawler', $actual);

        $this->assertSame($expected, $this->formatter->formatting($dom)->html());
    }
}
