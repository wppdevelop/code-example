<?php

namespace App\Tests\unit\Util\DOM;

use App\Util\DOM\DOMWrapperInterface;
use App\Util\DOM\FactoryWrapper;
use Codeception\Test\Unit;

class DOMFactoryTest extends Unit
{
    /**
     * @throws \Exception
     */
    public function testBuildException(): void
    {
        $this->expectExceptionMessage('Class "App\Util\DOM\FakeWrapper" not found');
        (new FactoryWrapper())->build('fake', '');
    }

    public function classDataProvider(): array
    {
        return [
            ['crawler']
        ];
    }

    /**
     * @param $class
     * @dataProvider classDataProvider
     * @throws \Exception
     */
    public function testBuild($class): void
    {
        $dom = (new FactoryWrapper())->build($class, '');

        $this->assertContainsOnlyInstancesOf(DOMWrapperInterface::class, [$dom]);
    }
}
