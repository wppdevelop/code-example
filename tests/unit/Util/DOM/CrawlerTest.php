<?php

namespace App\Tests\unit\Util\DOM;

use App\Util\DOM\CrawlerWrapper;
use App\Util\DOM\DOMWrapperInterface;
use App\Util\DOM\FactoryWrapper;
use Codeception\Test\Unit;

class CrawlerTest extends Unit
{
    /** @var DOMWrapperInterface */
    private $dom;

    /**
     * @throws \Exception
     */
    protected function _before(): void
    {
        $this->dom = (new FactoryWrapper)->build('crawler', '<div><span class="span">test</span></div>');
    }

    public function testContainsInstanceOfInterface(): void
    {
        $this->assertContainsOnlyInstancesOf(DOMWrapperInterface::class, [$this->dom]);
    }

    public function testContainsInstanceOfClass(): void
    {
        $this->assertInstanceOf(CrawlerWrapper::class, $this->dom);
    }

    public function testFind(): void
    {
        $this->assertCount(1, $this->dom->find('.span'));
    }

    public function testHtml(): void
    {
        $this->assertSame('<div><span class="span">test</span></div>', $this->dom->html());
    }

    public function emptyHtmlDataProvider(): array
    {
        return [
            ['', ''],
            ['', null],
        ];
    }

    /**
     * @dataProvider emptyHtmlDataProvider
     * @throws \Exception
     */
    public function testEmptyHtml($expected, $actual): void
    {
        $this->assertSame($expected, (new FactoryWrapper)->build('crawler', $actual)->html());
    }
}
