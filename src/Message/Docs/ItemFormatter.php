<?php


namespace App\Message\Docs;


class ItemFormatter
{
    /** @var string */
    private $docSlug;

    /** @var string */
    private $docLanguageSlug;

    /** @var string */
    private $docVersionSlug;

    /** @var string */
    private $docItemSlug;

    public function __construct(string $docSlug, string $docLanguageSlug, string $docVersionSlug, string $docItemSlug)
    {
        $this->docSlug = $docSlug;
        $this->docLanguageSlug = $docLanguageSlug;
        $this->docVersionSlug = $docVersionSlug;
        $this->docItemSlug = $docItemSlug;
    }

    public function getDocSlug(): string
    {
        return $this->docSlug;
    }

    public function getDocLanguageSlug(): string
    {
        return $this->docLanguageSlug;
    }

    public function getDocVersionSlug(): string
    {
        return $this->docVersionSlug;
    }

    public function getDocItemSlug(): string
    {
        return $this->docItemSlug;
    }
}