<?php

namespace App\MessageHandler\Docs;

use App\Entity\Docs\DocItem;
use App\Message\Docs\ItemFormatter;
use App\Repository\Docs\DocItemRepository;
use App\Service\FormatterService;
use App\Util\DOM\FactoryWrapper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Queue processing item formatter
 * @package App\MessageHandler\Docs
 */
class ItemFormatterHandler implements MessageHandlerInterface
{
    /** @var DocItemRepository */
    private $docItemRepository;

    /** @var EntityManagerInterface */
    private $em;

    /** @var FormatterService */
    private $formatterService;

    /** @var FactoryWrapper */
    private $factoryWrapper;

    public function __construct(
        DocItemRepository $docItemRepository,
        EntityManagerInterface $em,
        FormatterService $formatterService,
        FactoryWrapper $factoryWrapper
    )
    {
        $this->docItemRepository = $docItemRepository;
        $this->em = $em;
        $this->formatterService = $formatterService;
        $this->factoryWrapper = $factoryWrapper;
    }

    /**
     * @param ItemFormatter $itemFormatter
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(ItemFormatter $itemFormatter)
    {
        $docItem = $this->getDocItem($itemFormatter);

        if ($docItem->getBodyHtml()) {
            $docItem->setBodyHtml(
                $this
                    ->formatterService
                    ->getGroup('default')
                    ->formatting($this->factoryWrapper->build('crawler', $docItem->getBodyHtml()))
                    ->html()
            );

            $this->em->flush();
        }
    }

    /**
     * @param ItemFormatter $itemFormatter
     * @return DocItem
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getDocItem(ItemFormatter $itemFormatter): DocItem
    {
        return $this->docItemRepository->findOneByDocLanguageVersionItemSlug(
            $itemFormatter->getDocSlug(),
            $itemFormatter->getDocLanguageSlug(),
            $itemFormatter->getDocVersionSlug(),
            $itemFormatter->getDocItemSlug()
        );
    }
}