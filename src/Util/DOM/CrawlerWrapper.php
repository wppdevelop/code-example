<?php


namespace App\Util\DOM;


use Symfony\Component\CssSelector\CssSelectorConverter;
use Symfony\Component\DomCrawler\Crawler;
use DOMElement;
use ArrayIterator;

/**
 * Crawler Wrapper
 * @package App\Util\DOM
 */
class CrawlerWrapper implements DOMWrapperInterface
{
    /** @var Crawler */
    private $crawler;

    /** @var CssSelectorConverter */
    private $cssSelector;

    /**
     * Crawler constructor.
     * @param string|null $html
     */
    public function __construct(?string $html)
    {
        $this->crawler = new Crawler((string)$html);
        $this->cssSelector = new CssSelectorConverter();
    }

    /**
     * @param string $selector
     * @return ArrayIterator|DOMElement[]
     */
    public function find(string $selector)
    {
        return $this->crawler->filterXPath($this->cssSelector->toXPath($selector))->getIterator();
    }

    /**
     * @return string
     */
    public function html(): string
    {
        return $this->crawler->count()
            ? $this->crawler->filter('body')->html()
            : '';
    }
}