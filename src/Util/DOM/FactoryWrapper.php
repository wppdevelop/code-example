<?php


namespace App\Util\DOM;


use RuntimeException;

/**
 * Factory for receiving house wrapper
 * @package App\Util\DOM
 */
class FactoryWrapper
{
    /**
     * @param string $class
     * @param string|null $html
     * @return DOMWrapperInterface
     */
    public function build(string $class, ?string $html): DOMWrapperInterface
    {
        $class = __NAMESPACE__ . '\\' . ucfirst($class) . 'Wrapper';
        if (!class_exists($class)) {
            throw new RuntimeException('Class "' . $class . '" not found');
        }

        return new $class($html);
    }
}