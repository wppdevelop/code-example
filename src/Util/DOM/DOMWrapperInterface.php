<?php


namespace App\Util\DOM;


use DOMElement;
use ArrayIterator;

/**
 * Interface DOMWrapperInterface
 * @package App\Util\DOM
 */
interface DOMWrapperInterface
{
    /**
     * Search for DOM elements
     * @param string $selector
     * @return ArrayIterator|DOMElement[]
     */
    public function find(string $selector);

    /**
     * Returns html
     * @return string
     */
    public function html(): string;
}