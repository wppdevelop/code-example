<?php


namespace App\Util\HtmlFormatter;


use App\Util\DOM\DOMWrapperInterface;

/**
 * Class AbstractHtmlDecorator
 * @package App\Util\HtmlFormatter
 */
class AbstractHtmlDecorator implements HtmlDecoratorInterface
{
    /** @var HtmlDecoratorInterface */
    protected $formatter;

    public function __construct(HtmlDecoratorInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * Formatting DOM elements
     * @param DOMWrapperInterface $dom
     * @return DOMWrapperInterface
     */
    public function formatting(DOMWrapperInterface $dom): DOMWrapperInterface
    {
        return $this->formatter->formatting($dom);
    }
}