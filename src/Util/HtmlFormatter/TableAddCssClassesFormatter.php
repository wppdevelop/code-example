<?php


namespace App\Util\HtmlFormatter;


use App\Util\DOM\DOMWrapperInterface;

/**
 * Adding CSS classes for table tag
 * @package App\Util\HtmlFormatter
 */
class TableAddCssClassesFormatter extends AbstractHtmlDecorator
{
    /**
     * @inheritDoc
     * @param DOMWrapperInterface $dom
     * @return DOMWrapperInterface
     */
    public function formatting(DOMWrapperInterface $dom): DOMWrapperInterface
    {
        parent::formatting($dom);

        foreach ($dom->find('table') as $element) {
            $element->setAttribute('class', 'table table-striped boxed');
        }

        return $dom;
    }
}