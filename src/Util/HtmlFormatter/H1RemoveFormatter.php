<?php


namespace App\Util\HtmlFormatter;


use App\Util\DOM\DOMWrapperInterface;

/**
 * H1 tag removal
 * @package App\Util\HtmlFormatter
 */
class H1RemoveFormatter extends AbstractHtmlDecorator
{
    /**
     * @inheritDoc
     * @param DOMWrapperInterface $dom
     * @return DOMWrapperInterface
     */
    public function formatting(DOMWrapperInterface $dom): DOMWrapperInterface
    {
        parent::formatting($dom);

        foreach ($dom->find('h1') as $element) {
            $element->parentNode->removeChild($element);
        }

        return $dom;
    }
}