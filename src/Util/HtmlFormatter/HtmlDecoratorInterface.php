<?php


namespace App\Util\HtmlFormatter;


use App\Util\DOM\DOMWrapperInterface;

/**
 * Interface HtmlDecoratorInterface
 * @package App\Util\HtmlFormatter
 */
interface HtmlDecoratorInterface
{
    /**
     * Formatting DOM elements
     * @param DOMWrapperInterface $dom
     * @return DOMWrapperInterface
     */
    public function formatting(DOMWrapperInterface $dom): DOMWrapperInterface;
}