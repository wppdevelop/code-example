<?php


namespace App\Util\HtmlFormatter;

use App\Util\DOM\DOMWrapperInterface;

/**
 * Adding CSS classes for H2-H6 tags
 * @package App\Util\HtmlFormatter
 */
class HeadingAddCssClassesFormatter extends AbstractHtmlDecorator
{
    /**
     * @inheritDoc
     * @param DOMWrapperInterface $dom
     * @return DOMWrapperInterface
     */
    public function formatting(DOMWrapperInterface $dom): DOMWrapperInterface
    {
        parent::formatting($dom);

        foreach ($dom->find('h2,h3,h4,h5,h6') as $element) {
            $element->setAttribute('class', 'section-title-2');
        }

        return $dom;
    }
}