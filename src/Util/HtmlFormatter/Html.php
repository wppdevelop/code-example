<?php


namespace App\Util\HtmlFormatter;


use App\Util\DOM\DOMWrapperInterface;

/**
 * The main element of formatting. Returns the text "as is"
 * @package App\Util\HtmlFormatter
 */
class Html implements HtmlDecoratorInterface
{
    /**
     * @inheritDoc
     * @param DOMWrapperInterface $dom
     * @return DOMWrapperInterface
     */
    public function formatting(DOMWrapperInterface $dom): DOMWrapperInterface
    {
        return $dom;
    }
}