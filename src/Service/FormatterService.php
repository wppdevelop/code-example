<?php

namespace App\Service;

use App\Util\HtmlFormatter\Html;
use App\Util\HtmlFormatter\HtmlDecoratorInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Getting a group of fortmaters
 * @package App\Service
 */
class FormatterService
{
    /** @var array */
    private $params;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->params = $parameterBag->get('html_formatter');
    }

    /**
     * @param string $name
     * @return HtmlDecoratorInterface
     */
    public function getGroup(string $name): HtmlDecoratorInterface
    {
        $formatter = new Html();
        foreach ($this->getFormatters($name) as $class) {
            $formatter = new $class($formatter);
        }

        return $formatter;
    }

    /**
     * @param string $name
     * @return array
     */
    protected function getFormatters(string $name): array
    {
        if (!isset($this->params['groups'][$name])) {
            throw new RuntimeException('Group "' . $name . '" not found in html_formatter.yml');
        }

        return $this->params['groups'][$name];
    }
}